---
layout: markdown_page
title: Product Direction - Fulfillment
description: "The Fulfillment Team at GitLab focus create and support the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2021-08-19

## Fulfillment Section Overview

The Fulfillment section is passionate about creating seamless commerce experiences for our customers. We traverse [sales segments](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation), checkout preferences, and hosting options focused on ensuring customers can easily purchase, activate, and manage their subscriptions. Our goal is to increase net ARR while improving overall GTM efficiency, doing so will help GitLab scale growth and create an exponential lift while other departments and sections leverage our platform and experiences.

Currently our section is divided across three groups. Purchase is responsible for our primary checkout experiences and any product-driven purchase automation, License is responsible for provisioning of both SaaS and Self-Managed subscriptions, and Utilization takes care of usage reporting and usage admin controls. We also collaborate frequently across all of GitLab to achieve our goals. Most commonly we work with Sales Ops, Commercial and Enterprise Sales, Enterprise Applications, Growth and the Data teams. 

If you have any feedback on our direction we'd love to hear from you. Feel free and raise an MR, open an issue, or email Justin Farris. 

## Principles

Across our stable counterparts we follow four key principles (listed below). These principles keep us focused on delivering the right results and aide in enabling us to collaborate better with our stakeholders. These principles are not absolute, the intent is for them to guide our decision making. 

### Conducting business with GitLab should be seamless

When customers choose to purchase GitLab they've already discovered value in the product, they know what they want and they're ready to unlock additional value by accessing the features / services enabled by a transaction. We should strive to ensure our transacting experiences fade into the background. This creates a better customer experience (no one wants to labor over buying something, they just want to use the product they bought), and result in accelerated growth for GitLab. 

### Build a strong foundation so GitLab can scale and other teams can collaborate more effectively

Fulfillment's systems are often the foundational layer for a lot of the commerce conducted within the organization. We interface directly with Zuora (our SSOT for all subscriptions and transactions), maintain the licensing system that provisions a purchase for customers, and are the source of data for many KPIs and trusted data models. These systems need to be reliable, scale with demand and provide a solid surface area for other teams to collaborate on top of. If we do this well teams at GitLab don't need our time and resources as they take a dependency on our systems. 

### Use data to make decisions and measure impact

On the Fulfillment team we have many sensing mechanisms at our disposal: customers are a zoom call away, we sync with our counterparts across the business weekly, and our issue tracker is full of improvement suggestions raised by GitLab team members and members of the wider community. We're also beginning to improve how we use data as a sensing mechanism to set direction and prioritization. Understanding our funnel is paramount in building a seamless commerce experience for our customers, to do so the Fulfillment teams will ensure we've properly instrumented each point in our transaction funnels and leverage analysis of that data to inform our strategy and direction. 


### Iterate, especially when the impact of a change is sizeable

Iteration is one of the most challenging values to follow, especially within Fulfillment. Often times our work needs to be bundled and aligned closely with external announcements or communication. Even if that is unavoidable the Fulfillment team strives to break work down as much as possible and always be searching for the smallest possible iteration that delivers value to our customers or the business. 

## Composition

#### Purchase
The Purchase group is responsible for all transaction experiences and commerce automation. Trials, purchase flows, consumption purchasing, and account management (invoices, contact, billing info). This includes payments for new customers along with renewals. This group is also responsible for the purchase transitions and communication between our storefront and Zuora. Systems: [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot), [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab). Integrations: [Zuora](/handbook/engineering/development/fulfillment/architecture/#zuora), [Stripe](/handbook/engineering/development/fulfillment/architecture/#stripe).

#### License
The License group is responsible for all of the post-purchase activities that occur behind the scenes when a customer purchases new seats or modifies existing subscription. This includes retrieving and managing licenses, provisioning of SaaS subscriptions, and integrations to back of house tools. Systems: [LicenseDot](/handbook/engineering/development/fulfillment/architecture/#licensedot), Salesforce, [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab).

#### Utilization
The Utilization group is responsible for all consumables reporting and management, usage reporting, and usage notifications. [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab).

## Vision

Our vision is to build an exceptional commerce experience for our customers, an experience that "gets out of the way" of the product and enables GitLab to scale growth as we add more and more customers. Delivering on this vision will mature all interfaces where customers conduct business with GitLab, regardless of customer size or needs they'll be able to transact smoothly and begin using what they've purchased quickly and efficiently. By focusing on building an exceptional experience we'll also enable GitLab team members contributing to GTM activities. Sales can scale to spend more time on accounts with a [high LAM](https://about.gitlab.com/handbook/sales/sales-term-glossary/#landed-addressable-market-lam), while functions like Support and Finance will spend less time manually supporting customers and our field teams. 

To achieve this vision we'll focus on the following areas:

- Build a best-in-class webstore
- Drive more transactions to self-service
- Deliver competitive purchasing options in sales-assisted and webstore orders
- Enable channel partners and distributors to deliver experiences as good as our direct sales motions
- Make license and user management fully self-service

### Build a best-in-class webstore

Many new logos land as small first-orders. A common buyer persona in the webstore is [Alex](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) choosing to purchase GitLab for their team. Alex may work at a small startup poised to grow, or a larger enterprise with some influence to adopt a new DevOps tool. Either way that initial experience with purchasing GitLab often starts in the webstore as a self-service transaction. This customer isn't necessarily ready to talk with sales, and only wants to make a small investment to get started with GitLab. By building a simple and easy to use transaction experience we can get out of Alex's way and get them back to [adopting newly acquired features and onboarding](https://about.gitlab.com/direction/growth/#drive-feature-adoption-and-usage). One of the best ways to do this is reducing friction from the point a customer has decided to purchase to when they're back in app developing their applications. This is why we're focused first on [moving our purchase flows](https://gitlab.com/groups/gitlab-org/-/epics/1888) out of a separate website (customers.gitlab.com) and into the core product. This forms the foundation that enables us to start iterate from. Our goal is for many other teams to contribute to that purchase experience, namely our Growth team counterparts who might run experiments improving conversion. Furthermore, we also aim to provide optionality at checkout and make the purchase experience tie more closely to the entire GTM self-service funnel. As one example: Imagine being able to run promotions that offer discounts connected to a marketing campaign, making that experience seamless will help improve conversion and lead to a better overall experience transacting with GitLab. 

### Drive more self-service transactions

The focus here is efficiency, enabling more and more transactions to be started and completed via the webstore will free up our GTM counterparts to spend more time on higher value activities. Naturally the most benefit we get here is from SMB accounts but the vision is to enable support across our entire customer base. Mid-Market and Enterprise customers may want to transact completely self-service OR add-on to their GitLab subscription with incremental spend (e.g. purchase additional CI minutes). The fastest and most efficient way to do this for both the customer and our field teams is to enable that capability digitally. To do this well everyone in the loop needs visibility into the transactions. Our field teams may be compensated from those transactions and the customer needs a clear understanding of their total spend on GitLab, even if some of those transactions originated via different "storefronts". 

### Deliver competitive purchasing options in sales-assisted and webstore orders

While the majority of GitLab transactions occur with a credit card and pay up-front for anything they purchase, that's not necessarily the preferred method for a large part of the world. Outside of the US the preferred digital payment method is an e-wallet and many customers as they grow and scale have more complex payment and billing requirements. To meet global demand we need to support multiple payment types and options. This can include multiple payment types, support for complex billing motions automatically via the webstore (POs, ACH transactions, etc), and support for multiple currencies. 

### Enable channel partners and distributors to deliver experiences as good as our direct sales motions

More and more GitLab customers begin their journey with GitLab via a partner. They may transact in a cloud provider's marketplace or purchase with a larger bundling of software via a distributor. Our goal is to ensure those customers and partners get as high quality of service as they would buying direct. This means extending our APIs to support "indirect" transactions and collaborating closely with our counterparts in Channel Ops, Finance, and Enterprise Apps to design solutions that extend our internal systems beyond their direct-sales use-cases. 

### Make license, consumption and user-management fully self service

With GitLab 14.1 we launched [Cloud Licensing](https://docs.gitlab.com/ee/subscriptions/self_managed/#cloud-licensing) this provides a foundation for an improved licensing and provisioning experience for our customers. For years GitLab was behind in modernizing our licensing systems and architecture, now that we've caught up we need to continue to iterate to help customers scale and reduce friction in license management. There are two key areas to focus on in the future: 

- **Enterprise License Management** The larger the instance the more challenging license and seat management becomes. These customers often need multiple instances, move large amounts of users in and out of the application each month and have complicated configuration requirements. To that end we will begin to unpack these problems and deliver solutions that help customers with larger instances easily deploy licenses and manage provisioning over the entire enterprise. The goal is to ensure [Sidney](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator) and [Skyler](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#skyler---the-chief-information-security-officer) can easily deploy licenses to meet the evolving needs of their business. 

- **Usage and Reporting** The most common Fulfillment-related questions customers ask after they've transacted is related to usage and consumption. While the majority of GitLab transactions occur on an annual subscription model there is still incremental usage and spend customers need to manage each month or quarter. This comes in the form of add on users, and consumables (storage and CI minutes). Managing usage based spend can be a headache for any customer. Organizations of any size need predictability in costs so our goal is to deliver transparent and clear reporting of seat usage, consumption and any other metered usage/billing option GitLab offers. 

## Fulfillment Section's Current Focus (FY22 Q3)
Fulfillment just wrapped up a large cross-functional effort to deliver Cloud Licensing. After a smooth and successful rollout we're shifting focus towards a few key initiatives:
*Note: this work is not inclusive of all Epics/Issues scheduled in the next few milestones, to see that list please see our more detailed [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=start_date_asc&label_name%5B%5D=section%3A%3Afulfillment)*

**All Groups**
* [Infra Dev - Move customers.gitlab.com out of Azure and into GCP](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/81509)

**Purchase**
* Followups from Cloud Licensing
* Quarterly Reconciliation support for customers who require purchase orders (including Channel partners)
* Support to complete the migration to Zuora orders
* Problem and Solution validation work to drive more SMB transactions in the webstore

**License**
* Followups and bug fixes for Cloud Licensing
* Support for Zuora orders migration
* [GitLab Hosted Provisioning Improvements](https://gitlab.com/groups/gitlab-org/-/epics/5012)
* Problem and Solution validation for enterprise license management

**Utilization**
* User Caps
* Improvements to Billable Members
* Seat Usage Vision
* Project Level Storage Visibility

## Performance Indicators
Below is a list of performance indicators we track amongst the Fulfillment team. Note: some metrics are non-public so they may not be visible in the handbook. 

**[Percentage of transactions through self-service purchasing](https://about.gitlab.com/handbook/product/performance-indicators/#percentage-of-transactions-through-self-service-purchasing)**

**[Percentage of SMB transactions through self-service purchasing](https://app.periscopedata.com/app/gitlab/779889/WIP:-Percentage-of-SMB-transactions-through-self-service-purchasing?widget=10313311&udv=0)**

**[CI Minutes Transactions and Revenue](https://app.periscopedata.com/app/gitlab/744221/CI-minutes-Pricing-Initiative)**

**[Storage Consumption](https://app.periscopedata.com/app/gitlab/768903/GitLab-Storage)**


## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=section%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

We also track and prioritize work closely with our GTM counterparts. This is tracked in an internal [spreadsheet](https://docs.google.com/spreadsheets/d/1JdtaZYO90pR4_NQgSZRu9qdGuMrFj_H6qkBs5tFMeRc/edit#gid=0) and reviewed monthly with the CRO leadership team. 
