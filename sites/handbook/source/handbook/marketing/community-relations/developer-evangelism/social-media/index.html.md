---
layout: handbook-page-toc
title: "Developer Evangelism on Social Media"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Developer Evangelism builds out their thought leadership through social media and community engagement. The tips and strategies shared here can be used by team members and the wider community to help build their own profile as an evangelist.

Topics:

- Education and Learning: Tips from own experience. Workshops, slides, blog posts, videos, etc.
- Events live tweets / tweet storms. Amplify talks with screenshots and messages. 
- [Release Evangelism](#release-evangelism): Share feature insights with personal views. 
- Community best practices and GitLab insights

## Target Platforms

Twitter and LinkedIn are the target platforms. Both platforms have different target audiences and content distribution.

> Wil Spillane in Slack:
>
> Tweets are more fleeting. I think a lot of folks choose to view tweets in reverse chronological order. For LinkedIn, the default is “top” posts and it’s not very visible to many to switch to “recent”. Also, LinkedIn shows posts with recent activity in the recent feed, not just the plain reverse chronological order that Twitter does. So in some ways, LinkedIn posts linger across feeds longer than a tweet. 

### Message Tips

- Keep the message short and appealing. If you have multiple sentences, break them down into a list.
  - 💡 Use emojis as list markers, this one to learn something. 
  - 🏗 This way users learn what you want to share and build together.
  - 🔥 Pick the right emoji, this one expresses a fast success for example. 
- Use 3-4 hashtags at maximum, at the end of the message. Twitter/LinkedIn are not Instagram and use different algorithms to amplify shares.
  - `#development` and `#DevSecOps` are good examples, avoid them to include in every tweet though. 
- Too many emojis can hide key messages.
- Use an appealing screenshot image or funny animated GIF to make people stop when scrolling. 
- Do not start with an `@` character on Twitter, this can be hidden as reply and hinder audience reach on Twitter. Escape it with a leading `.` or an emoji. Example: `.@gitlab 13.9 adds ...`.

### Fast Emojis Workflows

You can use [Alfred's Powerpack with the emoji workflow](https://dev.to/dnsmichi/emojis-everywhere-supercharged-with-alfred-workflows-1o3n) to quickly access common emojis for tweets. 

Alternatively, [Better Tweetdeck](https://better.tw/) offers the same auto-complete mechanism known from GitLab and Slack: Start with a `:` and type the emoji name to start the live search. 

## Content

### Scheduled Content

You can use [Tweetdeck for scheduling tweets](https://help.twitter.com/en/using-twitter/advanced-tweetdeck-features).

#### Buffer 

Developer Evangelists use [Buffer](https://buffer.com/) in the [Pro tier](https://buffer.com/pricing/publish) to manage scheduled campaigns. Buffer acts as a queue with async publishing functionality. This allows to collect interesting URLs from your browser and mobile, add them to the queue and publish in different time slots throughout the day/week. 

Note that you can `@` mention Twitter accounts only, the LinkedIn integration does not match. Buffer also allows to upload/override a social image for better visual appearances. 

Helpful Buffer extensions/apps:

- [Chrome extension](https://chrome.google.com/webstore/detail/buffer/noojglkidnpfjbincgijbaiedldjfbhh?hl=en): Right click on a page and select `Buffer This Page`. Shortcut on macOS: `option + b`. 
- [iOS app](https://apps.apple.com/us/app/buffer-social-media-manager/id490474324): Share with app functionality.
- [Android app](https://play.google.com/store/apps/details?id=org.buffer.android): Share with app functionality.

Read more in [this article](https://buffer.com/library/save-time-social-media-scheduling/).

### Live Content

Buffer can be used to "live tweet" with the `Share now` functionality.

#### Twitter

- [Twitter web app](https://twitter.com/home)
- [TweetDeck](https://tweetdeck.twitter.com/) with the [Better TweetDeck](https://better.tw/) extension for a column based view
  - Auto-completed emojis, GIFs and media preview
  - Add more columns with custom searches, list subscriptions and notifications

The TweetDeck search allows for logical expressions:

```
gitlab OR gitlabcommit
```

#### LinkedIn

- [Web app](https://www.linkedin.com/)

### Content Sources

- Twitter lists
  - [@dnsmichi](https://twitter.com/dnsmichi/lists) 
- Newsletters
  - [cron.weekly](https://ma.ttias.be/cronweekly/) (Linux, Ops)
  - [The New Stack](https://thenewstack.io/newsletter-archive/) (Cloud Native, DevOps)
  - [Heavybit](https://www.heavybit.com/subscribe/) (Monitoring/Observability)
  - [Cassidy Williams - cassidoo](https://cassidoo.co/newsletter/) (Dev)
  - [allesnurgegloud.com](https://allesnurgecloud.com/) (Cloud, German)
- Blogs & News
  - [opensource.com](https://opensource.com/)
  - [DZone](https://dzone.com/)
  - [Geeker's digest](https://www.geekersdigest.com/)
- [dev.to Following tags](https://dev.to/dashboard/following_tags)
- [Hacker News](https://news.ycombinator.com/)
- [GitLab Forum](https://forum.gitlab.com/)
  - Buffer can consume RSS feeds, for example `https://forum.gitlab.com/c/questions-and-answers.rss`.
- GitLab Slack: [#external-comms](https://gitlab.slack.com/archives/CB274TZRR), [#newswire](https://gitlab.slack.com/archives/CERAPFN7R), [#competition](https://gitlab.slack.com/archives/C1BBL1V3K), [#hn-mention](https://gitlab.slack.com/archives/CBL93C22D), [#ceo](https://gitlab.slack.com/archives/C3MAZRM8W)

## Analytics

Please see [Metrics Collection & Analysis](/handbook/marketing/community-relations/developer-evangelism/metrics/) for our methods and tools.

## Thought Leadership Strategy

This strategy can help amplify impressions and build out thought leadership.

- Attract more active followers and therefore improve impression numbers and engagements.
  - Users can follow you on LinkedIn, you do not need to accept every invitation. If you plan to extend your business network, ensure that profile details such as private email/phone are not shared with anyone. 
- Help and educate users
- Analyse profile statistics
  - The why on the most impressions, top media tweet or most engaging tweets
- Follow users who share interesting stories
  - They may follow back, increasing the follower count.
- Retweet with comment and add your own thoughts or a funny emoji.
  - Mix this with "normal" retweets.
- Engage with tweets, like often, add replies and join the discussion.
  - Say `Thanks for sharing :emoji:` whenever needed
  - Share positive vibes
  - Follow the [team member social media policy](/handbook/marketing/team-member-social-media-policy/)
- Listen to criticism and ignore hate speech. 
- Do not criticize GitLab competitors. 
  - Instead, engage with their communities and learn how to improve.  
- Channel back feedback to product and engineering teams.
- Adopt new ideas with live streaming or community coffee chats. 
  - Engage community members in discussions.

Example: Chromium build times discussion led to [self-hosted GitLab with auto-scaling runners](https://twitter.com/dnsmichi/status/1351241954349223944) on Monday. On Wednesday, we decided to try it [live on YouTube](https://twitter.com/dnsmichi/status/1351944765697363974) in the [#everyonecancontribute cafe](https://www.youtube.com/watch?v=isKaBJ4VT24). Max wrote a [blog post](https://fotoallerlei.com/blog/post/2021/13-everyonecancontribute-cafe-autoscaling-gitlab-runners-on-hetzner-cloud/post) afterwards. 

### Build your Social Profile

- Add a personal note and allow users to view a window into your life. Be it food, leisure activities, or a quirky habit. 
- Share your impressions and thoughts with `#allremote` and `#remotework`
- Regularly tweet about daily work. Use the hash tag `#LifeAtGitLab` to share insights and funny moments.
- Pick outstanding GitLab features from another stage/group and post about them (could be a blog post, screenshot, etc.). 
  - Share praise in Slack with linking the Tweet/LinkedIn URL.
- Post something funny, use an animated GIF which relates to a tech story. Something which makes you laugh when you look at it.

When you find something interesting to be shared with our brand account, please share it on Slack in [#social_media_action](https://gitlab.slack.com/archives/C01AZ9C8Z4G) and retweet by yourself too.


## Social Campaigns

### Release Evangelism

[GitLab releases](/releases/) add lots of value every month. As a developer, you know about use cases and workflow enhancements they may solve specifically. Use this knowledge to describe **why** this new feature helps you.

The [product kick-off](/direction/kickoff/) provides insights into planned features. As we plan ambitiously, there is no guarantee that everything gets released as planned. Therefore release evangelism campaigns need to happen short notice around the 22nd each month. 

> **Tip**: GitLab team members can access the [#release-post](https://gitlab.slack.com/archives/C3TRESYPJ) Slack channel to check for updates on the monthly release blog post.
> Wider community members can check the `gitlab-com/www-gitlab-com repository` for the [release post MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?label_name%5B%5D=release+post), or follow the Developer Evangelism issues with the [DE-Release-Evangelism](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues?label_name[]=DE-Release-Evangelism) label. 

Workflow:

- Our [DE Bot](/handbook/marketing/community-relations/developer-evangelism/projects/#developer-evangelism-bot) automatically creates and assigns issues.
- DEs review the release blog post draft. 
  - Use the review app.
  - This also helps with direct content feedback for the release post managers. 
- DEs schedule their favourites.
  - Use the existing media (images, videos) from the release blog post.
  - Link to the documentation, or the release post anchor if already released. **Never link to the review app.**
- Impressions are collected with Keyhole, and happen async. 

Tips:

- 1-2 shares stretched over 1 week around the release
  - If you are on PTO, engage later. 
- Message format: `Problem? -> Solution`
- Add emojis and a CTA URL. 
- Animated Gifs or videos can help with people stopping "doom scrolling". 
- Target a time window for PT and CEST (9-11am PT)

KPIs:

- Impressions and engagement

#### Release Evangelism Examples

High performing share examples:

- [13.12: Dynamic CI/CD variables based on conditions on Twitter](https://twitter.com/dnsmichi/status/1396129739622998022) (23,000 impressions, 2,300 engagements)
- [13.11: Tweetstorm to highlight 13.11 and tease 13.12](https://twitter.com/dnsmichi/status/1395686623132758017) (32,000 impressions)
- [13.10: Nested CI/CD variables on Twitter](https://twitter.com/dnsmichi/status/1383014333500813322) (18,000 impressions, 1,100 engagements) [LinkedIn](https://www.linkedin.com/posts/dnsmichi_gitlab-1310-allows-to-nest-cicd-variables-activity-6787693633425424384-WelD) (2,300 views, 35 reactions)
- [13.10: Parallel CI/CD job execution in child pipeline triggers on Twitter](https://twitter.com/dnsmichi/status/1382351735960797184) (13,000 impressions, 700 engagements) [LinkedIn](https://www.linkedin.com/posts/dnsmichi_parallel-cicd-job-execution-in-gitlab-improves-activity-6788117887266652160-hNIV) (1,100 views, 25 reactions)
- [13.9: Expanded CI/CD configuration on Twitter](https://twitter.com/dnsmichi/status/1363879804785803268) (5,000 impressions, 400 engagements)
- [13.9: SAST with .NET 5.0](https://www.linkedin.com/posts/dnsmichi_gitlab-dotnet-activity-6772576735876714496-noR_) (1,600 views, 43 reactions)
- [13.8: CI pipeline editor on LinkedIn](https://www.linkedin.com/posts/dnsmichi_best-release-ever-gitlab-inc138-is-here-activity-6758427487870451712-TttR) (5,000 views, 10 comments, 316 reactions, 15 reshares)
- [13.8: Download CI/CD job artifacts in the MR widget on LinkedIn](https://www.linkedin.com/posts/dnsmichi_gitlab-activity-6758714643964805120-9Yyd) (800 views, 14 reactions)
- [13.7: Merge Request Reviewers on LinkedIn](https://www.linkedin.com/posts/dnsmichi_gitlab-devops-productivity-activity-6756907989321433088-TRW9) (500 views, 16 reactions)
- [13.7: Rendered URLs in CI/CD job logs on LinkedIn](https://www.linkedin.com/posts/dnsmichi_everyonecancontribute-activity-6723982784492273664-cwg0) (4,800 views, 350 reactions, 12 comments)
- [13.6: VS Code GitLab workflow on Twitter](https://twitter.com/dnsmichi/status/1336362663107063808) (16,000 impressions, 700 engagements)
- [13.5: Group wikis on Twitter](https://twitter.com/dnsmichi/status/1319656105249820672) (18,000 impressions, 600 engagements)
- [13.4: Pipeline Efficiency docs on LinkedIn](https://www.linkedin.com/posts/dnsmichi_on-a-personal-note-my-first-larger-contribution-activity-6714235003447853056-Kkv4) (2,100 views, 49 reactions)
- [13.3: KubeCon Kubernetes teaser](https://twitter.com/dnsmichi/status/1296051268448915456) (18,000 impressions, 500 engagements)
- [13.3: Matrix builds of parallel CI/CD job execution on LinkedIn](https://www.linkedin.com/posts/dnsmichi_gitlab-development-cicd-activity-6702933595721351168-c-nV) (2,400 views, 49 reactions)
- [13.1: Individual CI/CD job artifcat reports on LinkedIn](https://www.linkedin.com/posts/dnsmichi_gitlab-cicd-development-activity-6683033972479451136-VNVL) (700 views, 16 reactions)

#### Wider community involvement is encouraged

- Ask GitLab heroes in [Gitter](https://gitter.im/gitlab/heroes) to share their favourite upcoming feature on social.
- Share the release MR with the review app, including this handbook URL for tips and best practices.

### Product Feedback

Next to our user research campaigns and feedback in the project issues, we often see feedback and interesting ideas on social media. Sometimes users tag our brand account `@gitlab`, in other scenarios we discover them with searching for `gitlab`.

With moving this into a direct question like `If there would be one feature you could add, what would it be?`, this can help identify stakeholders and encourages for a more direct feedback loop.

This effort needs a cross-team collaboration between product & engineering, social and DE teams. Assigned DE DRI is [Michael Friedrich](/company/team/#dnsmichi).

Workflow:

- Create a new DE request issue
- Define the topic/scope
- Propose a message for social, max. 280 characters.
- Schedule the social share & document
- Monitor the social share for responses
- Move the specific responses to #product in Slack

Second iteration: Make this a self-service with a request form, and automated response collection/updates in Slack or similar. 

KPIs: 

- Number of engagements
- Issues updated/created

### Team Evangelism

Move social shares into product channels, be it good feedback or additional experts required to answer. Tag GitLab team members and encourage them to respond/discuss on social media when they can benefit from the conversation. For heated discussions, stay within a small group of social media DRIs.

For incoming questions and involving more experts, Developer Evangelists can help in Slack in [#developer-evangelism](https://gitlab.slack.com/archives/CMELFQS4B).

#### Release Management

Example tweets for [GitLab 12.9](/releases/2020/03/22/gitlab-12-9-released/):

- Pick a nice title and encourage everyone to try to create a new release: `Have you created your first release through the in @gitlab yet? Let's do this!`
- Go to your demo environment and create 1-4 screenshots, including guiding steps (make this a mini tutorial)
- URL to the release blogpost should be included
- Mention @gitlab with the @ before the username
- Use hashtags `#gitlab #releasemanagement`

#### Package

Example tweets for [open-sourcing the registries](/blog/2020/03/30/new-features-to-core/):

- Pick a cool title to encourage users to solve a use-case for them: `Unstable npm mirrors no more: @gitlab got you covered - the NPM registry will be open sourced.`
- Go to your demo environment and create 1-4 screenshots, including guiding steps (make this a mini tutorial)
- URL to the release blogpost should be included
- Mention @gitlab with the @ before the username
- Use hashtags: `#gitlab #packages #registry`




