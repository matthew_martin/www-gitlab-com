title: DevOps
description: Learn about the benefits and features of the DevOps lifecycle, and
  how to use communication and collaboration to deliver better quality code,
  faster!
header_body: If you want to build better software faster, DevOps is the answer.
  Here’s how this software development methodology brings everyone to the table
  to create secure code quickly.
canonical_path: /topics/devops/
file_name: devops
twitter_image: /images/opengraph/gitlab-blog-cover.png
related_content:
  - title: Test your DevOps platform knowledge
    url: https://about.gitlab.com/quiz/devops-platform/
  - title: Create the ideal DevOps team structure
    url: /topics/devops/build-a-devops-team/
  - title: The role Agile plays in DevOps
    url: https://about.gitlab.com/topics/agile-delivery/
  - title: Continuous integration in DevOps
    url: https://about.gitlab.com/topics/ci-cd/benefits-continuous-integration/
  - title: Understand continuous integration and delivery
    url: https://about.gitlab.com/topics/ci-cd/
  - title: What is GitOps?
    url: https://about.gitlab.com/topics/gitops/
  - title: Understand DevSecOps
    url: https://about.gitlab.com/topics/application-security/
cover_image: /images/topics/devops-lifecycle.png
body: >-
  ## What is DevOps?


  DevOps can be best explained as people working together to conceive, build and deliver secure software at top speed. DevOps practices enable software developers (devs) and operations (ops) teams to accelerate delivery through automation, collaboration, fast feedback, and iterative improvement.


  Stemming from an [Agile approach](https://about.gitlab.com/topics/agile-delivery/) to software development, a DevOps delivery process expands on the cross-functional approach of building and shipping applications in a faster and more iterative manner. In adopting a DevOps development process, you are making a decision to improve the flow and value delivery of your application by encouraging a more collaborative environment at all stages of the development cycle.


  > **DevOps** is a combination of software developers (dev) and operations (ops). It is defined as a software engineering methodology which aims to integrate the work of software development and software operations teams by facilitating a culture of collaboration and shared responsibility.


  DevOps represents a change in mindset for IT culture. In building on top of Agile, lean practices, and systems theory, DevOps focuses on incremental development and rapid delivery of software. Success relies on the ability to create a culture of accountability, improved collaboration, empathy, and joint responsibility for business outcomes.


  ## What is a DevOps platform?


  DevOps brings the human siloes together and an [DevOps platform](/solutions/devops-platform/) does the same thing for tools. Many teams start their [DevOps journey](https://page.gitlab.com/webcast-simplify-devops.html) with a disparate collection of tools, all of which have to be maintained and many of which don’t or can’t integrate. A DevOps platform brings tools together in a single application for unparalleled collaboration, visibility, and development velocity. A DevOps platform is how modern software should be created, secured, released, and monitored in a repeatable fashion. A truly “open” DevOps platform means just that: Teams can iterate faster and innovate together because everyone can contribute. 


  ## DevOps Culture


  The business value of DevOps lies in the ability to improve the production environment in order to deliver software faster with continuous improvement. You need the ability to anticipate and respond to industry disruptors without delay. This becomes possible within an Agile software development process where teams are empowered to be autonomous and deliver faster, reducing work in progress. Once this occurs, teams are able to respond to demands at the speed of the market.


  There are some fundamental concepts that need to be put into action in order for DevOps to function as designed, including the need to:


  * Remove institutionalized silos and handoffs that lead to road blocks and constraints, particularly in instances where the measurements of success for one team is in direct odds with another team’s key performance indicators (KPIs).

    * For example, Team A is tasked with preventing outages. In order to reach that goal, they create processes that are prohibitive to rapid changes in the application’s development; thereby creating a bottleneck that negatively impacts Team B’s KPI of frequent software updates.
  * Implement a unified tool chain using a single application that allows multiple teams to share and collaborate. This will enable teams to accelerate delivery and provide fast feedback to one another.


  ## DevOps Fundamentals


  DevOps covers a wide range of practices across the application lifecycle. Customers start with one or more of these practices in their journey to DevOps success.


  * **Source Code Management** - Teams looking for better ways to manage changes to documents, software, images, large web sites, and other collections of code, configuration, and metadata among disparate teams.

  * **Agile Project & Portfolio Management** - Teams looking for a better way of initiating, planning, executing, controlling, and closing the work of a team to achieve specific goals and meet specific success criteria at the specified time.

  * **Continuous Integration (CI)** - Teams looking for ways to automate the build and testing processes to consistently integrate code and continuously test to minimise the manual efforts spent in frequent runs of unit and integration tests.

  * **Continuous Delivery (CD)** - Teams looking for ways to automate the build, test and packaging, configuration and deployment of applications to a target environment.

  * **Shift Left Security** - Teams looking for ways to identify vulnerabilities during development with actionable information to empower dev to remediate vulnerabilities earlier in the lifecycle have specific goals and meet specific success criteria at the specified time.

  * **Monitoring and Feedback** - Teams looking for ways to embed monitoring into every deployed version and the impact of application changes to the business value and user experience.

  * **Rapid Innovation** - Teams looking for ways to provide feedback back into the development, test, packaging & deployment stages to complete the loop to integrate dev and ops teams and provide real time feedback from production environments and customers.
benefits_title: Benefits of DevOps
benefits_description: >-
  [Adopting a DevOps mode](/customers/axway-devops/)l breaks down barriers so
  that development and operations teams are no longer siloed and have a more
  efficient way to work across the entire development and application
  lifecycle.Without DevOps, organizations experience handoff friction, which
  delays the delivery of software releases and negatively impacts business
  results.


  The DevOps model is an organization’s answer to increasing operational efficiency, accelerating delivery, and innovating products. Organizations that have implemented a DevOps culture experience the benefits of increased collaboration, fluid responsiveness, and shorter cycle times.
benefits:
  - title: Collaboration
    description: Adopting a DevOps model creates alignment between development and
      operations teams; handoff friction is reduced and everyone is all in on
      the same goals and objectives.
    image: /images/icons/collaboration-icon.svg
  - title: Fluid responsiveness
    description: More collaboration leads to real-time feedback and greater
      efficiency; changes and improvements can be implemented quicker and
      guesswork is removed.
    image: /images/icons/efficiency-icon.svg
  - title: Shorter cycle time
    description: Improved efficiency and frequent communication between teams
      shortens cycle time; new code can be released more rapidly while
      maintaining quality and security.
    image: /images/icons/time-icon.svg
cta_banner:
  - body: >-
      ### Starting and Scaling DevOps in the Enterprise


      Sharing his pioneering insight on how organizations can transform their software development and delivery processes, Gary Gruver provides a tactical framework to implement DevOps principles in “Starting and Scaling DevOps in the Enterprise.”
    cta:
      - url: https://about.gitlab.com/resources/scaling-enterprise-devops/
        text: Download your free copy
    title: Start your DevOps journey
resources_title: Resources
resources_intro: >-
  Here's a list of resources on DevOps that we find to be particularly helpful
  in understanding DevOps and implementation. We would love to get your
  recommendations on books, blogs, videos, podcasts and other resources that
  tell a great DevOps story or offer valuable insight on the definition or
  implementation of the practice.


  Please share your favorites with us by tweeting us [@gitlab](https://twitter.com/gitlab)!
resources:
  - title: How DevOps leads transformation (GitLab Virtual Commit 2020 track)
    url: https://www.youtube.com/playlist?list=PLFGfElNsQthbAbiHjRVNz1WwxbhLfeXXs
    type: Video
  - title: Cloud-Native DevOps (GitLab Virtual Commit 2020 track)
    url: https://www.youtube.com/playlist?list=PLFGfElNsQthb4FD4y1UyEzi2ktSeIzLxj
    type: Video
  - title: DevOps tips and tricks (GitLab Virtual Commit 2020 track)
    url: https://www.youtube.com/playlist?list=PLFGfElNsQthZ_LGh4EpGJduNd2nFhN5fn
    type: Video
  - title: " How to simplify DevOps"
    url: https://www.youtube.com/watch?v=TUwvgz-wsF4
    type: Video
  - type: Case studies
    url: /customers/axway-devops/
    title: Axway aims for elite DevOps status
  - title: Worldline and the importance of collaboration
    url: https://about.gitlab.com/customers/worldline/
    type: Case studies
  - title: The European Space Agency and DevOps
    url: https://about.gitlab.com/customers/european-space-agency/
    type: Case studies
  - title: GitLab’s 2020 Global DevSecOps Survey
    url: /developer-survey/
    type: Reports
  - title: Gartner on application release orchestration
    url: https://about.gitlab.com/blog/2020/01/16/2019-gartner-aro-mq/
    type: Reports
  - url: https://www.arresteddevops.com/
    title: Arrested DevOps
    type: Podcast
  - title: Leading the Transformation
    url: https://www.amazon.com/Leading-Transformation-Applying-DevOps-Principles/dp/1942788010
    type: Books
  - url: https://www.amazon.com/The-Goal-Process-Ongoing-Improvement/dp/0884271951/
    title: The Goal
    type: Books
  - url: https://www.amazon.com/Start-Scaling-Devops-Enterprise-Gruver/dp/1483583589/
    title: Starting and Scaling DevOps in the Enterprise
    type: Books
  - url: https://www.amazon.com/The-Phoenix-Project-Helping-Business/dp/0988262509/
    title: The Phoenix Project
    type: Books
suggested_content:
  - url: /blog/2019/10/07/auto-devops-explained/
  - url: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
  - url: /blog/2020/09/30/leading-scm-ci-and-code-review-in-one-application
  - url: /blog/2020/10/07/vcc-with-a-single-app/
  - url: /blog/2020/10/30/future-proof-your-developer-career/
  - url: /blog/2020/10/29/gitlab-hero-devops-platform/
schema_faq:
  - question: What is DevOps?
    answer: DevOps can be best explained as people working together to build,
      deliver, and run resilient software at the speed of their particular
      business. DevOps practices enable software development (Dev) and
      operations (Ops) teams to accelerate delivery through automation,
      collaboration, fast feedback, and iterative improvement.
    cta:
      - url: https://about.gitlab.com/topics/devops/#what-is-devops:~:text=What%20is%20DevOps%3F
        text: Learn more about DevOps
  - question: What is a DevOps platform?
    answer: DevOps brings the human siloes together and a DevOps platform does the
      same thing for tools.
    cta:
      - text: Learn more about DevOps platform
        url: https://about.gitlab.com/topics/devops/#what-is-a-devops-platform:~:text=What%20is%20a%20DevOps%20platform%3F
